# wallpaper-changer
Wallpaper changer for GNOME 3 on CentOS 7

## How to Use

1. Edit the wallpaper-changer-su.sh and wallpaper-changer.sh files to match your username and wallpaper directory location:

`$ vim wallpaper-changer-su.sh`

`$ vim wallpaper-changer.sh` 

2. Copy the wallpaper-changer-su.sh and wallpaper-changer.sh files to /usr/local/bin:

`$ sudo cp wallpaper-changer-su.sh /usr/local/bin`

`$ sudo cp wallpaper-changer.sh /usr/local/bin`

`$ sudo chmod +x /usr/local/bin/wallpaper-changer*`

3. Decide to run this script via cron (A) or systemd (B):

### A - Cron

4. Run the wallpaper-changer.cron file to add the cronjob for your user:

`$ ./wallpaper-changer.cron`

5. Verify the cronjob exists:

`$ crontab -l`

### B - Systemd

> NOTE: This currently does not work as expected. The timer will not start on boot despited being enabled.
> It is recommended that you use the cron option for now.

4. Copy the wallpaper-change.service and .timer files to /etc/systemd/system:

`$ sudo cp wallpaper-changer.{service, timer} /etc/systemd/system`

5. Enable and start the timer file:

`$ sudo systemctl enable wallpaper-changer.timer`

`$ sudo systemctl start wallpaper-changer.timer`

## Note

The default duration for wallpaper changes is every 5 minutes. To change this, edit the cronjob or the .timer file.

Because CentOS 7 does not allow the use of 'systemd --user', I have had to set up the systemd part to run under root and use sudo to run the wallpaper-changer.sh script as my user. I'll try to improve this in the near future, but for now it does what it needs to do.
