#!/bin/bash
# author: Jeff Reeves
# purpose: wallpaper changer for Gnome 3 via cron
# how to use: either create a cronjob or a systemd 
#    time and service to execute this script as often
#    as you'd like
# notes: 
#   - a big thanks to Major Hayden's article (https://major.io/2015/06/23/)

# get user
USER=jeff

# wallpapers directory
WallpaperDIR="/home/${USER}/Pictures/wallpapers/"

# change directory to prevent error with find command
cd ${WallpaperDIR}

# get a random image in the directory
RandomImg=$(find ${WallpaperDIR} -type f -name "*.jpg" -o -name "*.png" | shuf -n1)

# build the URI where the chosen wallpaper is 
PictureURI="file://${RandomImg}"

# get the DBUS SESSION of the user
PID=$(pgrep -u $USER gnome-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)


# set the wallpaper
gsettings set org.gnome.desktop.background picture-uri "${PictureURI}"
