#!/bin/bash
# purpose: installs the wallpaper changer with default settings
# TODO: prompt user for username, wallpaper directory, etc.

# user defined variables
USER='jeff'
INSTALL_DIR='/usr/local/bin'
CURRENT_DIR=$(pwd)

# copy scripts to desired location
echo "[TASK] Copying wallpaper changer scripts to ${INSTALL_DIR}..."
sudo -u root bash << EOF
cp ${CURRENT_DIR}/wallpaper-changer-su.sh ${CURRENT_DIR}/wallpaper-changer.sh ${INSTALL_DIR}
chmod +x ${INSTALL_DIR}/wallpaper-changer.sh ${INSTALL_DIR}/wallpaper-changer-su.sh
EOF
echo "[SUCCESS] Wallpaper changer scripts copied"


# install cronjob
echo "[TASK] Creating wallpaper changer cron job..."

# get a list of all cronjobs, and add our new one too it
CRON_EXISTING=$(crontab -l)

if [[ ${CRON_EXISTING} =~ ".*wallpaper-changer.sh$" ]]; then 
	echo "[WARN] /var/spool/cron/${USER} already contains wallpaper-changer.sh cronjob"
else
    crontab -l | { cat; echo "*/5 * * * * ${INSTALL_DIR}/wallpaper-changer.sh"; } | crontab -
    if [ $? -eq 0 ]; then
        echo "[SUCCESS] Wrote cronjob to cron file at: /var/spool/cron/${USER}"
    fi
fi

# change to first wallpaper
echo "[TASK] Changing wallpaper for the first time..."
"${INSTALL_DIR}/wallpaper-changer.sh"

echo "[COMPLETE] Enjoy your wallpapers rotating every five minutes! =)"
